<?php
require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload

$file = isset($argv[1]) ? $argv[1] : false;

/** Validation */
if(!$file)
{
  echo "File name required eg. php main.php BackupReport_Jan_2016.xlsx" . PHP_EOL;
  exit;
}
$file = __DIR__ . "/../data/" . $file;
if(!file_exists($file))
{
  echo "File $file doesn't exist" . PHP_EOL;
  exit;
}

echo "Process Xls" . PHP_EOL;
$excelReader = \PHPExcel\IOFactory::createReaderForFile($file);
$excelObj = $excelReader->load($file);

$generator = new \ExcelParser\Generator($excelObj);
$sheets = $generator->processXlsSheets();

echo "Produce Html" . PHP_EOL;
$htmlPages = $generator->getSheetsHtml($sheets);

$views = __DIR__ . '/../templates';
$cache = __DIR__ . '/../cache';

$blade = new \Philo\Blade\Blade($views, $cache);
$content = $blade->view()->make('htmlTemplate', ['pages' => $htmlPages])
  ->render();

echo "Create Html File" . PHP_EOL;
$htmlFile = fopen(__DIR__ . '/../html/index.html', 'w');
fwrite($htmlFile, $content);
/**
 *
 */
