<?php
namespace ExcelParser;

use PHPExcel\Cell;
use PHPExcel\Spreadsheet;

class Generator
{
  /**
   * @var $excelObject Spreadsheet
   */
  private $excelObject;

  private $tableTemp = "<table id='%s' class='table'>%s</table>";
  private $trTemp = "<tr id='%s' class='tr'>%s</tr>";
  private $tdTemp = "<td id='%s' class='td'>%s</td>";

  public function __construct(Spreadsheet $excelObject)
  {
    $this->excelObject = $excelObject;
  }

  /**
   * @return array
   */
  public function processXlsSheets()
  {
    //get all sheet names from the file
    $worksheetNames = $this->excelObject->getSheetNames();
    $sheets = array();
    foreach($worksheetNames as $key => $sheetName)
    {
      echo "Sheet: " . $sheetName . PHP_EOL;
      //set the current active worksheet by name
      $this->excelObject->setActiveSheetIndexByName($sheetName);
      $objSheet = $this->excelObject->getActiveSheet();
      $highestRow = $objSheet->getHighestRow();
      $highestColumn = $objSheet->getHighestColumn();
      $rows = $objSheet->getRowDimensions();

      foreach($rows as $row)
      {
        if($row->getVisible())
        {
          list($rangeStart, $rangeEnd) = Cell::rangeBoundaries(
            'A' . $row->getRowIndex(
            ) . ':' . $highestColumn . $row->getRowIndex()
          );
          $minCol = Cell::stringFromColumnIndex($rangeStart[0] - 1);
          $maxCol = Cell::stringFromColumnIndex($rangeEnd[0] - 1);
          $maxCol++;
          // Loop through rows
          $c = -1;
          for($col = $minCol; $col != $maxCol; ++$col)
          {
            $cRef = (true) ? $col : ++$c;
            $cellVal = $objSheet->getCell($cRef . $row->getRowIndex())
              ->getCalculatedValue();
            echo $cRef . $row->getRowIndex() . '=' . $cellVal . ' ; ';

            $sheets[$sheetName][$row->getRowIndex()][$cRef] = $cellVal;
          }
        }
      }

      echo PHP_EOL;
    }
    return $sheets;
  }

  /**
   * @param $sheets
   * @return array
   */
  public function getSheetsHtml($sheets)
  {

    $pages = [];
    foreach($sheets as $sheetName => $sheet)
    {
      $page = new \stdClass();
      $trs = '';
      $tableId = strtolower(preg_replace('/[^\da-z]/i', '', $sheetName));

      foreach($sheet as $row => $cols)
      {
        $tds = '';

        foreach($cols as $index => $data)
        {
          $tdId = $sheetName . '-' . $row . $index;
          $tds .= sprintf($this->tdTemp, $tdId, $data);
        }
        $trId = $sheetName . '-' . $row;
        $trs .= sprintf($this->trTemp, $trId, $tds) . PHP_EOL;
      }
      $table = sprintf($this->tableTemp, $tableId, $trs) . PHP_EOL;
      $page->id = $tableId;
      $page->title = "$sheetName" . PHP_EOL;
      $page->table = $table;
      $pages[] = $page;
    }
    return $pages;
  }
}
