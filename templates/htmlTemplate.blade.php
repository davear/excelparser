<html>
<head>
  <link rel="stylesheet" href="assets/main.css">
</head>
<body>
@foreach($pages as $page)
<div id="{{ $page->id }}">
<h2>{{ $page->title }}</h2>

{!! $page->table !!}
</div>
<hr />
@endforeach

</body>

</html>
